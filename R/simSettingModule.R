simSettUI <- function(id) {
  tagList(
    bsModal(id = NS(id,"sim_setting_modal"), title = "Simulation Setting", trigger = "sim_setting_box_css", size = "medium",
            textInput(inputId = NS (id,"tgrid_start"),label = "Start",value =0 ),
            textInput(inputId = NS (id,"tgrid_end"),label = "End",value = 300),
            textInput(inputId = NS (id,"tgrid_delta"),label = "Delta",value = 1),
            textInput(inputId = NS (id,"tgrid_add"),label = "Add",value = 0),
            textInput(inputId = NS (id,"num_subj"),label = "Number of Subject", value = 1000),
            textInput(inputId = NS (id,"num_ite"),label = "Number of Iteration", value = 1),
            textInput(inputId = NS (id,"seed"),label = "Seed",value = 123456)
    )
  )
}

simSettServer <- function(id,simSettList,tgrid_list, simsetting_print_li) {
  moduleServer(id, function(input, output, session) {
    
    observeEvent(input$tgrid_start, {
      tgrid_list$start <- input$tgrid_start %>% as.numeric()
      simsetting_print_li$tgrid_start <- input$tgrid_start
      })
    observeEvent(input$tgrid_end, {
      tgrid_list$end <- input$tgrid_end %>% as.numeric()
      simsetting_print_li$tgrid_end <- input$tgrid_end
      })
    observeEvent(input$num_subj,{
      simSettList$num_subj <- input$num_subj %>% as.numeric()
      simsetting_print_li$num_subj <- input$num_subj
      })
    observeEvent(input$num_ite,{
      simSettList$num_ite <- input$num_ite %>% as.numeric()
      simsetting_print_li$num_ite <- input$num_ite
      })
    
    observeEvent(input$tgrid_delta, {tgrid_list$delta <- input$tgrid_delta %>% as.numeric()})
    observeEvent(input$tgrid_add, {tgrid_list$add <- input$tgrid_add %>% as.numeric()})
    observeEvent(input$seed,{simSettList$seed <- input$seed %>% as.numeric()})

  })
}