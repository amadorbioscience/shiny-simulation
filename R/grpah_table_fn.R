# simulated dataframe of 1 subject 1 iteration
graph_indiSimulation <- function (dataset, x = "time", y = "C"){
  ggplot (data = dataset, aes (x = !!sym (x), y = !!sym (y))) + 
    geom_line(color = "#11A79B") +
    labs (x = "Time", y = "Concentration") + 
    theme_bw()
}

# Output need column : time, SIM_NAME, Median, LowCI, HighCI, 
dataset_summary_graph_combine <- function (dataset, con_int, time_unit ){
  if (con_int == "90 %"){
    cil <- .05
    ciu <- .95
  }
  if (con_int == "95 %"){
    cil <- .025
    ciu <- .975
  }
  
  if (time_unit == "Day"){
    time_unit <- 1
  }
  if (time_unit == "Week"){
    time_unit <- 1/7
  }
  
  
  sum_res <- dataset %>%
    mutate (SIM_NAME = factor(SIM_NAME, levels = unique(dataset$SIM_NAME)),
            time = time * time_unit)%>% 
    group_by(time, SIM_NAME) %>% 
    summarise(Median = median (C), 
              LowCI = quantile(C, cil), 
              HighCI = quantile(C, ciu)) %>% 
    ungroup () %>% as.data.frame()
  return (sum_res)
}

color_pals <- function(cols) {
  pals <- c ("#00665E", "#644E92", "#824E19") # 100S 40B
  #pals <- c ("#6BB3AD", "#D191CF","#ACAA5B") # 40S 70B
  #pals <- c ("#54BFB6","#9976CC","#B77A38") # 56S 75B
  return (cols)
}

color_pallate_fn_graph_combine <- function (n, cols){
  pals <- color_pals (cols)
  if (n <= 3){
    return (list (scale_color_manual(values= pals), scale_fill_manual(values= pals)))
  } else {return (NULL)}
}

# graph simulated dataframe of x subject y iteration for multi simulation 
graph_combine <- function (sum_res, con_int, 
                           x_label = "Time", y_label = "Concentration",
                           plt_theme,cols_var){
  
 # sum_res <- dataset_summary_graph_combine(dataset, con_int, time_unit)
  n <- length (unique (sum_res$SIM_NAME)) # number of simulation for color pallate 
  
  if (plt_theme){
    plt_th <- theme_bw()
  } else {
    plt_th <- theme_classic()
  }
  
  plt <- ggplot (sum_res , aes (x = time, y = Median, color = SIM_NAME, fill = SIM_NAME)) + 
    geom_line () +
    geom_ribbon (aes (ymin = LowCI, ymax = HighCI), alpha = 0.2) + 
    labs (x = x_label, 
          y = y_label, 
          color = paste0("Median (",con_int,")"), 
          fill = NULL) + 
    plt_th + 
    color_pallate_fn_graph_combine (n,cols_var)
  return (plt)
}

# time at last dosing event
last_dose_time_fn <- function (rx_syntax){
  last_time_df <- ev_rx (rx_syntax) %>% realize_addl() %>% as.data.frame()
  return (max (last_time_df$time, na.rm = T) )
}

# time at second to last dosing event
second_last_dose_time_fn <- function (rx_syntax){
  ev_df <- ev_rx(rx_syntax) %>% realize_addl()  %>% as.data.frame() 
  ev_df <- ev_df %>% filter (time < max (ev_df$time, na.rm = T) )
  return (max (ev_df$time, na.rm = T))
}

# finding a time at peak concentration for cmin summary statistic
cmin_peak_time_filtered_greater <- function (dataset) {
  dataset %>% group_by (ID, REPI, SIM_NAME) %>% 
    filter (time >= time[which.max (C)] ) %>% 
    ungroup () %>% 
    as.data.frame()
}

# finding a time at peak concentration for cmin summary statistic
cmin_peak_time_filtered_less <- function (dataset) {
  dataset %>% group_by (ID, REPI, SIM_NAME) %>% 
    filter (time <= time[which.max (C)] ) %>% 
    ungroup () %>% 
    as.data.frame()
}

# all filter for cmin summary statistic
all_filtered_cmin_fn <- function (dataset , rx_syntax){
  last_time <- last_dose_time_fn (rx_syntax)
  second_last_time <- second_last_dose_time_fn (rx_syntax)
  
  # filter from second to last peak to last dose
  df1 <- dataset %>% filter (time < last_time,
                            time >= second_last_time)
  df1 <- cmin_peak_time_filtered_greater (df1)
  
  # filter from last dose to the end
  df2 <- dataset %>% filter (time >= last_time)
  df2 <- cmin_peak_time_filtered_less (df2)
  
  # binding 
  df <- bind_rows (df1,df2)
  
  return (df)
}

cmin_tmin_each_sim_fn <- function (dataset, rx_syntax){
  dataset <- all_filtered_cmin_fn (dataset, rx_syntax)
  
  cmin_indi <- dataset %>% 
    group_by(ID, REPI, SIM_NAME) %>%
    summarise (Cmin = min (C, na.rm = T), 
               Tmin = time[which.min(C)],
               .groups = "drop") %>% 
    as.data.frame()
  
  cmin_indi <- cmin_indi %>% 
    group_by(REPI, SIM_NAME) %>%
    summarise( Cmin = median(Cmin, na.rm = TRUE),
               Tmin = median(Tmin, na.rm = TRUE),
               .groups = "drop") %>% 
    as.data.frame()
  
  cmin_indi %>% group_by (SIM_NAME) %>%
    summarise(Median_Cmin = median(Cmin, na.rm = T) %>% round (digits = 2),
              Mean_Cmin = mean(Cmin, na.rm = T)%>% round (digits = 2),
              SD_Cmin = sd(Cmin, na.rm = T)%>% round (digits = 2),
              Median_Tmin = median(Tmin, na.rm = T)%>% round (digits = 2),
              Mean_Tmin = mean(Tmin, na.rm = T)%>% round (digits = 2),
              SD_Tmin = sd(Tmin, na.rm = T)%>% round (digits = 2),
              .groups = "drop") %>%
    as.data.frame() %>% pivot_dataset_sumstat()
}

cmax_tmax_each_sim_fn <- function (dataset, rx_syntax){
  last_time <- last_dose_time_fn (rx_syntax)
  
  cmax_indi <- dataset %>% filter (time >= last_time) %>% 
    group_by(ID, REPI, SIM_NAME) %>%
    summarise (Cmax = max (C, na.rm = T), 
               Tmax = time [which.max(C)],
               .groups = "drop") %>% 
    as.data.frame()
  
  cmax_indi <- cmax_indi %>% 
    group_by(REPI, SIM_NAME) %>%
    summarise( Cmax = median(Cmax, na.rm = TRUE),
               Tmax = median(Tmax, na.rm = TRUE),
               .groups = "drop") %>% 
    as.data.frame()
  
  cmax_indi %>% group_by (SIM_NAME) %>%
    summarise(Median_Cmax = median(Cmax, na.rm = T) %>% round (digits = 2),
              Mean_Cmax = mean(Cmax, na.rm = T)%>% round (digits = 2),
              SD_Cmax = sd(Cmax, na.rm = T)%>% round (digits = 2),
              Median_Tmax = median(Tmax, na.rm = T)%>% round (digits = 2),
              Mean_Tmax = mean(Tmax, na.rm = T)%>% round (digits = 2),
              SD_Tmax = sd(Tmax, na.rm = T)%>% round (digits = 2),
              .groups = "drop") %>%
    as.data.frame() %>% pivot_dataset_sumstat()
}

auc_each_sim_fn <- function (dataset, interval){
  auc_indi <- dataset %>% filter (time >= interval [1], time <= interval [2]) %>%
    group_by (ID, REPI, SIM_NAME) %>% 
    summarise (AUC = auc (time, C),
               .groups = "drop") %>% 
    as.data.frame()
  
  auc_indi <- auc_indi %>% 
    group_by(REPI, SIM_NAME) %>%
    summarise( AUC = median(AUC, na.rm = TRUE),
               .groups = "drop") %>% 
    as.data.frame()
  
  auc_indi %>% 
    group_by(SIM_NAME) %>%
    summarise(Median_AUC = median(AUC, na.rm = T)%>% round (digits = 2),
              Mean_AUC = mean(AUC, na.rm = T)%>% round (digits = 2),
              SD_AUC = sd(AUC, na.rm = T)%>% round (digits = 2),
              .groups = "drop") %>% 
    as.data.frame() %>% pivot_dataset_sumstat() %>%
    mutate (Interval = paste0 (interval[1]," - ", interval[2]))
  
}

# change the structure for summary statistic
pivot_dataset_sumstat <- function (dataset){
  dataset %>% 
    pivot_longer(cols = -SIM_NAME,names_to = c(".value", "Parameter"), 
                 names_sep = "_", values_drop_na = FALSE) %>%
    as.data.frame()
}


binding_sumstat <- function(li,active_all_sims){
  res = data.frame()
  
  vec <- paste0("simulation_", active_all_sims)
  
  for (ele in vec){
    res <- bind_rows(res,li$cmax[[ele]],li$cmin[[ele]])
  }
  
  return (res)
}